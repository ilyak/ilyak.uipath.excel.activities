﻿using System;

namespace IlyaK.UiPath.Excel.Activities
{
    class Helper
    {
        /// <summary>
        /// format for R1C1-style cell reference
        /// </summary>
        public const string r1c1Format = "R{0}C{0}";
        /// <summary>
        /// RegExp group number for column
        /// </summary>
        public const int columnGrp = 1;
        /// <summary>
        /// RegExp group number for row
        /// </summary>
        public const int rowGrp = 2;
        /// <summary>
        /// RegExp pattern for Excel cell name
        /// </summary>
        public const string ExcelCellA1 = @"(\D+)(\d+)";
        public const string defaultCell = "A1";
        public const int alphabetLength = 26;
        public const int MaxRowsNumber = 1048576;
        public const int MaxColsNumber = 16384;
        public const int capitalA = (int)'A';
        public const int capitalZ = (int)'Z';
    }
}
