﻿using System.Activities.Presentation.Metadata;
using System.ComponentModel;


namespace IlyaK.UiPath.Excel.Activities.Design
{

    public class DesignerMetadata : IRegisterMetadata
    {

        public void Register()

        {

            AttributeTableBuilder attributeTableBuilder = new AttributeTableBuilder();
            attributeTableBuilder.AddCustomAttributes(typeof(IlyaK.UiPath.Excel.Activities.ConvertToRc), new DesignerAttribute(typeof(ConvertToRcDesigner)));
            attributeTableBuilder.AddCustomAttributes(typeof(IlyaK.UiPath.Excel.Activities.ConvertToA1), new DesignerAttribute(typeof(ConvertToA1Designer)));
            MetadataStore.AddAttributeTable(attributeTableBuilder.CreateTable());

        }

    }

}
