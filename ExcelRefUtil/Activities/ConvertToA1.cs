﻿using IlyaK.UiPath.Excel.Activities.Properties;
using System;
using System.Activities;
using System.ComponentModel;

namespace IlyaK.UiPath.Excel.Activities
{
    [LocalizedCategory(nameof(Resources.AppIntegrationExcel))]
    [LocalizedDisplayName(nameof(Resources.ConvertToA1DisplayName))]
    [LocalizedDescription(nameof(Resources.ConvertToA1Description))]
    public class ConvertToA1 : CodeActivity
    {
        [LocalizedCategory(nameof(Resources.Output))]
        [LocalizedDisplayName(nameof(Resources.CellName))]
        [LocalizedDescription(nameof(Resources.CellDescription))]
        [DefaultValue(Helper.defaultCell)]
        public OutArgument<String> Cell { get; set; }

        [LocalizedCategory(nameof(Resources.Input))]
        [LocalizedDisplayName(nameof(Resources.RowName))]
        [LocalizedDescription(nameof(Resources.RowDescription))]
        [RequiredArgument]
        [DefaultValue(1)]
        public InArgument<Int32> Row { get; set; }

        [LocalizedCategory(nameof(Resources.Input))]
        [LocalizedDisplayName(nameof(Resources.ColumnName))]
        [LocalizedDescription(nameof(Resources.ColumnDescription))]
        [RequiredArgument]
        [DefaultValue(1)]
        public InArgument<Int32> Column { get; set; }

        protected override void Execute(CodeActivityContext context)
        {
            Int32 row = Row.Get(context);
            if (row <= 1 || row > Helper.MaxRowsNumber)
            {
                throw new System.ArgumentException(Resources.RowInvalidFormat, Resources.RowName);
            }
            Int32 column = Column.Get(context);
            if (column < 1 || column > Helper.MaxColsNumber)
            {
                throw new System.ArgumentException(Resources.ColumnInvalidFormat, Resources.ColumnName);
            }
            Cell.Set(context, ColumnNumberToName(column).ToString() + row.ToString());
        }

        /// <summary>
        /// Converts column number into Excel column name in A1-style
        /// </summary>
        /// <param name="column">column number </param>
        /// <returns>A1-style column name </returns>
        protected string ColumnNumberToName(Int32 column) {
            string colstr = "";
            int modn;
            int num = column;
            do {
                num--;
                modn = num % Helper.alphabetLength;
                colstr = (char)(Helper.capitalA + modn) + colstr;
                num /= Helper.alphabetLength;
            } while (num > 0);
            return colstr;
        }
    }
}
