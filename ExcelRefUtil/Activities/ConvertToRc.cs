﻿using IlyaK.UiPath.Excel.Activities.Properties;
using System;
using System.Activities;
using System.ComponentModel;
using System.Text.RegularExpressions;

namespace IlyaK.UiPath.Excel.Activities
{
    [LocalizedCategory(nameof(Resources.AppIntegrationExcel))]
    [LocalizedDisplayName(nameof(Resources.ConvertToRcDisplayName))]
    [LocalizedDescription(nameof(Resources.ConvertToRcDescription))]
    public class ConvertToRc : CodeActivity
    {
        /// <summary>
        /// A1-style cell address (input parameter)
        /// </summary>
        [LocalizedCategory(nameof(Resources.Input))]
        [LocalizedDisplayName(nameof(Resources.CellName))]
        [LocalizedDescription(nameof(Resources.CellDescription))]
        [DefaultValue(Helper.defaultCell)]
        [RequiredArgument]
        public InArgument<String> Cell { get; set; }

        /// <summary>
        /// Row number (output parameter)
        /// </summary>
        [LocalizedCategory(nameof(Resources.Output))]
        [LocalizedDisplayName(nameof(Resources.RowName))]
        [LocalizedDescription(nameof(Resources.RowDescription))]
        public OutArgument<Int32> Row { get; set; }

        /// <summary>
        /// Column number (output parameter)
        /// </summary>
        [LocalizedCategory(nameof(Resources.Output))]
        [LocalizedDisplayName(nameof(Resources.ColumnName))]
        [LocalizedDescription(nameof(Resources.ColumnDescription))]
        public OutArgument<Int32> Column { get; set; }

        /// <summary>
        /// R1C1-style cell address (output parameter)
        /// </summary>
        [LocalizedCategory(nameof(Resources.Output))]
        [LocalizedDisplayName(nameof(Resources.R1C1))]
        [LocalizedDescription(nameof(Resources.R1C1Description))]
        public OutArgument<String> R1C1 { get; set; }
        
        /// <summary>
        /// Main function of the activity. Takes A1-format cell address
        /// and returns numeric column and row (and R1C1-format string)
        /// </summary>
        /// <param name="context">Activity context</param>
        protected override void Execute(CodeActivityContext context)
        {
            string input = Cell.Get(context);
            int row = 1;
            Match match = Regex.Match(input, Helper.ExcelCellA1);
            if (match.Groups.Count < 2) {
                throw new System.ArgumentException(Resources.CellInvalidFormat, Resources.CellName);
            }
            //getting column
            string columnName = match.Groups[Helper.columnGrp].Value.ToUpper();
            //getting row
            string rowName = match.Groups[Helper.rowGrp].Value;

            try
            {
                row = Convert.ToInt32(rowName);
            }
            catch
            {
                throw new System.ArgumentException(Resources.RowInvalidFormat, Resources.RowName);
            }
            finally
            {
                if (row <= 1 || row > Helper.MaxRowsNumber)
                {
                    throw new System.ArgumentException(Resources.RowInvalidFormat, Resources.RowName);
                }
            }

            int column = ColumnNameToNumber(columnName);
            R1C1.Set(context, String.Format(Helper.r1c1Format, row, column));
            Column.Set(context, column);
            Row.Set(context, row);
        }

        /// <summary>
        /// Converts the A1 column name into numeric column number
        /// </summary>
        /// <param name="columnName">Excel column name (like A or AB)</param>
        /// <returns>returns column number</returns>
        protected Int32 ColumnNameToNumber(string columnName)
        {
            Int32 column = 0;
            Int32 size = columnName.Length - 1;
            for (Int32 i = 0; i <= size; i++)
            {
                Int32 code = (int)(columnName[i]);
                if (code < Helper.capitalA || code > Helper.capitalZ)
                {
                    throw new System.ArgumentException(Resources.CellInvalidFormat, Resources.CellName);
                }
                code = (int)(columnName[i]) - Helper.capitalA + 1;
                Int32 mult = Helper.alphabetLength * (size - i);
                mult = i == size ? 1 : mult;
                column += code * mult;
            }
            return column;
        }
    }
}
