# ilyak.uipath.excel.activities

UiPath Excel Activities (starting with R1C1<->A1 address conversion helper)

Currently UiPath Excel activities do not support R1C1-style cell reference.
That makes it difficult to manipulate Excel ranges, as you cannot easly add or remove from column names such as AA or BZ.

Classic use cases are

*  I need to get 10 columns starting from column AB
*  Get 5th cell to the right from current cell
*  What will be a relevant column name in Excel for my DataTable column 7?

This activities packages was primarily created to demonstrate advanced custom activities concepts such as Designer, Localization and .nuspec creation.

This simple package adds two activities to UiPath Studio (both could be found at **App Integration -> Excel**)
1. `Convert to R1C1`, takes A1-style cell address ("A1" is an example, or "AZ120") and extracts numberic row and column values
2. `Convert to A1`, takes numberic row and column values and creates A1-style address